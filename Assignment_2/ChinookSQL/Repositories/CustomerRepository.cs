﻿using Assignment_2.ChinookSQL.Models;
//using Microsoft.Data.SqlClient;
using System.Data.SqlClient;

namespace Assignment_2.ChinookSQL.Repositories
{   /// <summary>
    /// Main class for all functionality
    /// </summary>
    public class CustomerRepository : ICustomerRepository
    {

        public List<Customers> GetAllCustomers()
        {
            List<Customers> customerList = new List<Customers>();
            string sql = "SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString())) // Connect
                {
                    connection.Open(); //Open connection
                    using (SqlCommand cmd = new SqlCommand(sql, connection))// Make a command
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader()) //Reader
                        {
                            while (reader.Read()) // Handle result
                            {
                                Customers customer = new Customers();
                                customer.CustomerID = reader.GetInt32(0);
                                customer.FirstName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                customer.LastName = reader.IsDBNull(2) ? null : reader.GetString(2);
                                customer.Country = reader.IsDBNull(3) ? null : reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.PhoneNumber = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }


        public bool AddNewCustomer(Customers customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@firstname, @lastname, @country, @postalcode, @phone, @email)";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@firstname", customer.FirstName);
                        cmd.Parameters.AddWithValue("@lastname", customer.LastName);
                        cmd.Parameters.AddWithValue("@country", customer.Country);
                        cmd.Parameters.AddWithValue("@postalcode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@phone", customer.PhoneNumber);
                        cmd.Parameters.AddWithValue("@email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
        }


        public Customers GetCustomer(int id)
        {
            Customers myCustomer = new Customers();
            string sql = "SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                "WHERE CustomerID = @CustomerID";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerID", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                myCustomer.CustomerID = reader.GetInt32(0);
                                myCustomer.FirstName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                myCustomer.LastName = reader.IsDBNull(2) ? null : reader.GetString(2);
                                myCustomer.Country = reader.IsDBNull(3) ? null : reader.GetString(3);
                                myCustomer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                myCustomer.PhoneNumber = reader.IsDBNull(5) ? null : reader.GetString(5);
                                myCustomer.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return myCustomer;
        }


        public bool UpdateCustomer(Customers customer)
        {
            bool success = false;
            string sql = "UPDATE Customer " +
                "SET FirstName = @firstname, LastName = @lastname, Country = @country, PostalCode = @postalcode, Phone = @phonenumber, Email = @email "
                +"WHERE CustomerID = @CustomerID ";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerID", customer.CustomerID);
                        cmd.Parameters.AddWithValue("@firstname", customer.FirstName);
                        cmd.Parameters.AddWithValue("@lastname", customer.LastName);
                        cmd.Parameters.AddWithValue("@country", customer.Country);
                        cmd.Parameters.AddWithValue("@postalcode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@phonenumber", customer.PhoneNumber);
                        cmd.Parameters.AddWithValue("@email", customer.Email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
        }


        public List<CustomerCountry> CountCountry()
        {
            List<CustomerCountry> countries = new List<CustomerCountry>();
            string sql = "SELECT Country, COUNT(*) as Total " +
                "FROM Customer " +
                "GROUP BY Country " +
                "ORDER BY Total DESC;";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry country = new CustomerCountry();
                                country.Country = reader.GetString(0);
                                country.Total = reader.GetInt32(1);
                                countries.Add(country);
                            }
                        }
                    }
                }
            }
            catch (InvalidCastException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return countries;
        }


        public List<CustomerSpender> HighInvoice()
        {
            List<CustomerSpender> spenders = new List<CustomerSpender>();
            string sql = "SELECT Customer.CustomerID, FirstName, LastName, SUM(Total) as Total " +
                "FROM Customer INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                "GROUP BY Customer.CustomerId, FirstName, LastName " +
                "ORDER BY Total DESC";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender spender = new CustomerSpender()
                                {
                                    CustomerID = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Total = reader.GetDecimal(3),
                                };
                                spenders.Add(spender);
                                
                            }
                        }
                    }
                }

            }
            catch (InvalidCastException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return spenders;
        }


        public List<CustomerGenre> PopularGenre(int id)
        {
            List <CustomerGenre> genres = new List<CustomerGenre>();
            string sql = "SELECT TOP 1 WITH TIES Customer.CustomerId, Customer.FirstName, Customer.LastName, Genre.Name, COUNT(*) AS Total " +
                "FROM((((Customer " +
                "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId) " +
                "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId) " +
                "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId) " +
                "INNER JOIN Genre ON Track.GenreId = Genre.GenreId) " +
                "WHERE Customer.CustomerId = @customerid " +
                "GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName, Genre.Name " +
                "ORDER BY Total DESC; ";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@customerid", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre genre = new CustomerGenre()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    CustomerFirstName = reader.GetString(1),
                                    CustomerLastName = reader.GetString(2),
                                    Name = reader.GetString(3),
                                    Total = reader.GetInt32(4),
                                };
                                genres.Add(genre);
                            }
                        }
                    }
                }
            }
            catch (InvalidCastException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return genres;
        }


        public List<Customers> GetCustomerByName(string name) //USES LASTNAME
        {
            List<Customers> customerList = new List<Customers>();
            string sql = "SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                "WHERE LastName LIKE @LastName";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@LastName", name);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customers customer = new Customers();
                                customer.CustomerID = reader.GetInt32(0);
                                customer.FirstName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                customer.LastName = reader.IsDBNull(2) ? null : reader.GetString(2);
                                customer.Country = reader.IsDBNull(3) ? null : reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.PhoneNumber = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                                customerList.Add(customer);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }


        public List<Customers> GetCustomerPage(int limit, int offset)
        {
            List<Customers> customerList = new List<Customers>();
            string sql = "SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                "ORDER BY CustomerID " +
                "OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY;";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@limit", limit);
                        cmd.Parameters.AddWithValue("@offset", offset);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customers customer = new Customers();
                                customer.CustomerID = reader.GetInt32(0);
                                customer.FirstName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                customer.LastName = reader.IsDBNull(2) ? null : reader.GetString(2);
                                customer.Country = reader.IsDBNull(3) ? null : reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.PhoneNumber = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }
    }
}













